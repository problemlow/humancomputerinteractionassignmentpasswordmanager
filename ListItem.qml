import QtQuick 2.6
import QtQuick 2.0
import "main.js" as JS


Rectangle {
    property int ident
    ident: index
    id: listItem1
    x: parent.width / 32 * .5
    y: parent.width / 32 * .5
    width: parent.width / 32 * 31
    height: parent.height / 11.75
    color: "#555555"

    Text {
        id: txtListItem
        x: 0
        y: 0
        width: parent.width / 16 * 4.75
        height: parent.height
        color: "#ffffff"
        text: "xx-xx-xx-xx"
        font.pixelSize: parent.height
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        id: box0
        x: parent.width / 16 * 4.75
        y: parent.height / 16
        width: parent.width / 16 * 7.25
        height: parent.height / 16 * 14
        color: "#ffffff"
        border.color: "#007700"
        TextInput {
            id: txtBox
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            maximumLength: 18
            horizontalAlignment: Text.AlignHCenter
            font.weight: Font.Normal
            text: ""
            echoMode: TextInput.Normal
            font.pixelSize: height / 8 * 6
        }
    }

    Rectangle {
        id: btnRemove
        x: parent.width / 16 * 12
        width: parent.width / 16 * 4
        height: parent.height
        color: "#000000"
        MouseArea {
            id: msAreaRemove
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            onClicked: {
                JS.processRemove(parent.parent.ident)
            }
        }

        Text {
            id: txtRemove
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            color: "#ffffff"
            text: qsTr("Remove")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: parent.height
        }
    }
}
