#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlContext>
#include "cppfunctions.h"
using namespace std;


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QScopedPointer<CPPFunctions> cpp(new CPPFunctions);
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    engine.rootContext()->setContextProperty("cpp", cpp.data());

    return app.exec();
}




