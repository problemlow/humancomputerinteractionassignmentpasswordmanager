function randomNumber(){
    var number = (Math.floor(Math.random() * 10)).toString()+(Math.floor(Math.random() * 10)).toString();
    return number;
}
function FFSCPP(char){
    char = char.charCodeAt(0)
    cpp.FFS(char)
}
function clearFile(){
    cpp.Blank()
}

function addToList(){
    var temp = txtBox0.text + "-" + txtBox1.text + "-" + txtBox2.text + "-" + txtBox3.text;
    if ((temp.length == 11)&&(txtBox0.text[0] != " ")&&(txtBox0.text[1] != " ")&&(txtBox1.text[0] != " ")&&(txtBox1.text[1] != " ")&&(txtBox2.text[0] != " ")&&(txtBox2.text[1] != " ")&&(txtBox3.text[0] != " ")&&(txtBox3.text[1] != " ")){
        var component = Qt.createComponent("ListItem.qml");
        var sprite = component.createObject(listItems, {"x": (parent.width / 32 * .5)-1, "y": (parent.height / 32 * (2.5*parseInt(listItemPlace.text)))-1+(parent.height/32)*.68, "ident": parseInt(indexReg.text)});
        listItems.children[parseInt(indexReg.text)].children[0].text = temp
        txtBox0.text = ""
        txtBox1.text = ""
        txtBox2.text = ""
        txtBox3.text = ""
        listItemPlace.text = parseInt(listItemPlace.text)+1
        indexReg.text = parseInt(indexReg.text)+1
        if (indexReg.text == "nan"){
            indexReg.text = listItemPlace.text
        }
    } else {
        console.log("Make sure all parts of the text you entered consist only of numbers and you have 2 numbers in each field")
    }
}

function processRemove(element){
    listItems.children[element].destroy();
    listItemPlace.text = parseInt(listItemPlace.text)-1
    element = element-1
    indexReg.text = parseInt(indexReg.text)-1

    if (element < 1){
        element = 0
    }
    while (listItems.children[element].ident != null){
        listItems.children[element].ident = parseInt(listItems.children[element].ident)-1
        listItems.children[element].y = listItems.children[element].y - (listItems.children[element].height*1.25)
        element = element+1
    }
    element = element-1
    if (element < 1){
        element = 0
    }
}

function saveTable(){
    clearFile()
    var iterate = 0;
    for (var i in listItems.children){
        var tempPassword = listItems.children[iterate].children[0].text;
        var tempHint = listItems.children[iterate].children[1].children[0].text;
        var dump = tempPassword + "," + tempHint + "\n";
        for (var iter = 0; iter < dump.length; iter++) {
          FFSCPP(dump.charAt(iter));
        }
        iterate = iterate+1
    }
}
