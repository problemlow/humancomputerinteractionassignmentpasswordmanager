import QtQuick 2.6
import QtQuick.Window 2.2

Window {
    visible: true
    width: 720
    height: 480
    title: qsTr("Human Computer Interaction Assessment: Password Manager")

    MainForm {
        anchors.fill: parent
    }
}
