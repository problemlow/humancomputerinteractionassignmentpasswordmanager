import QtQuick 2.6
import QtQuick 2.0
import "main.js" as JS

Rectangle {

    width: 720
    height: 640
    color: "#999999"

    Rectangle {
        id: box0
        x: parent.width / 32 * 8
        y: parent.height / 32
        width: parent.width / 16 * 2
        height: parent.height / 16
        color: "#ffffff"
        border.color: "#111111"
        TextInput {
            id: txtBox0
            x: parent.width / 2 - width / 2
            y: parent.height / 2 - height / 2
            width: (parent.width) * .9
            height: (parent.height) * .9
            maximumLength: 2
            horizontalAlignment: Text.AlignHCenter
            font.weight: Font.Normal
            inputMask: (99)
            text: ""
            //CLEAR THE TEXT BOX WHEN THE JS IS INTIALISED
            echoMode: TextInput.Normal
            font.pixelSize: height / 8 * 6
        }
    }
    Rectangle {
        id: box1
        x: parent.width / 32 * 13
        y: parent.height / 32
        width: parent.width / 16 * 2
        height: parent.height / 16
        color: "#ffffff"
        border.color: "#111111"
        TextInput {
            id: txtBox1
            x: parent.width / 2 - width / 2
            y: parent.height / 2 - height / 2
            width: (parent.width) * .9
            height: (parent.height) * .9
            maximumLength: 2
            horizontalAlignment: Text.AlignHCenter
            font.weight: Font.Normal
            inputMask: (99)
            text: ""
            //CLEAR THE TEXT BOX WHEN THE JS IS INTIALISED
            echoMode: TextInput.Normal
            font.pixelSize: height / 8 * 6
        }
    }
    Rectangle {
        id: box2
        x: parent.width / 32 * 18
        y: parent.height / 32
        width: parent.width / 16 * 2
        height: parent.height / 16
        color: "#ffffff"
        border.color: "#111111"
        TextInput {
            id: txtBox2
            x: parent.width / 2 - width / 2
            y: parent.height / 2 - height / 2
            width: (parent.width) * .9
            height: (parent.height) * .9
            maximumLength: 2
            horizontalAlignment: Text.AlignHCenter
            font.weight: Font.Normal
            inputMask: (99)
            text: ""
            //CLEAR THE TEXT BOX WHEN THE JS IS INTIALISED
            echoMode: TextInput.Normal
            font.pixelSize: height / 8 * 6
        }
    }

    Rectangle {
        id: box3
        x: parent.width / 32 * 23
        y: parent.height / 32
        width: parent.width / 16 * 2
        height: parent.height / 16
        color: "#ffffff"
        border.color: "#111111"
        TextInput {
            id: txtBox3
            x: parent.width / 2 - width / 2
            y: parent.height / 2 - height / 2
            width: (parent.width) * .9
            height: (parent.height) * .9
            maximumLength: 2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.weight: Font.Normal
            inputMask: (99)
            text: ""
            echoMode: TextInput.Normal
            font.pixelSize: height / 8 * 6
        }
    }

    Rectangle {
        id: btnAdd
        x: parent.width / 32 * 27.5
        y: parent.height / 32
        width: parent.width / 16 * 1.75
        height: parent.height / 16
        color: "#000000"
        MouseArea {
            id: msAreaAdd
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            onClicked: {
                JS.addToList()
            }
        }

        Text {
            id: txtAdd
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            color: "#ffffff"
            text: qsTr("Add")
            font.pixelSize: parent.height
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: btnRandom
        x: parent.width / 16 * .5
        y: parent.height / 32
        width: parent.width / 16 * 3.25
        height: parent.height / 16
        color: "#000000"
        MouseArea {
            id: msAreaRandom
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            onClicked: {
                txtBox0.text = JS.randomNumber()
                txtBox1.text = JS.randomNumber()
                txtBox2.text = JS.randomNumber()
                txtBox3.text = JS.randomNumber()
            }
        }

        Text {
            id: txtRandom
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            color: "#ffffff"
            text: qsTr("Random")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: parent.height
        }
    }

    Rectangle {
        id: btnSave
        x: parent.width / 16 * .5
        y: parent.height / 16 * 2
        width: parent.width / 16 * 6.75
        height: parent.height / 16
        color: "#000000"
        MouseArea {
            id: msAreaSave
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            onClicked: {
                JS.saveTable()
            }
        }

        Text {
            id: txtSave
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            color: "#ffffff"
            text: qsTr("Save")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: parent.height
        }
    }

    Rectangle {
        id: btnLoad
        x: parent.width / 16 * 8.75
        y: parent.height / 16 * 2
        width: parent.width / 16 * 6.75
        height: parent.height / 16
        color: "#000000"
        MouseArea {
            id: msAreaLoad
            x: 0
            y: 0
            width: parent.width
            height: parent.height
        }

        Text {
            id: txtLoad
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            color: "#ffffff"
            text: qsTr("Load")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: parent.height
        }
    }

    Rectangle {
        id: listItems
        x: parent.parent.width / 16 * .5
        y: parent.parent.height / 16 * 3.5
        width: parent.parent.width / 32 * 30
        height: parent.parent.height / 32 * 23.5
        color: "#ffffff"
    }

    Text {
        id: listItemPlace
        x: -10
        y: -10
        width: 0
        height: 0
        font.pixelSize: 0
        color: "#ffffff"
        text: "0"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Text {
        id: indexReg
        x: -10
        y: -10
        width: 0
        height: 0
        font.pixelSize: 0
        color: "#ffffff"
        text: "0"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Item {
        id: clock
        width: height
        height: 0
        x: parent.width / 2 - width / 2
        y: (parent.height / 16) * 1.5

        property int hours
        property int minutes
        property int seconds
        property real shift
        property bool night: true
        property bool internationalTime: true //Unset for local time

        function timeChanged() {
            if (height != (parent.height/8)){
                if (height > parent.height/8-1){
                    height = height-1
                }
                if (height < parent.height/8){
                    height = height+1
                }
            }
            var date = new Date
            txtDate.text = date.getDay() + "/" + date.getDate() + "/" + date.getFullYear();
            date = new Date(date)
            date = date.setHours(date.getHours());
            date = new Date(date)
            date = date.setSeconds(date.getSeconds());
            date = new Date(date)
            date = date.setMinutes(date.getMinutes());
            date = new Date(date)
            hours = internationalTime ? date.getUTCHours() + Math.floor(clock.shift):date.getHours()
            night = (hours < 7 || hours > 19)
            minutes = internationalTime ? date.getUTCMinutes() + ((clock.shift % 1) * 60):date.getMinutes()
            seconds = date.getUTCSeconds()
        }

        Timer {
            interval: 100
            running: true
            repeat: true
            onTriggered: clock.timeChanged()
        }

        Item {
            anchors.centerIn: parent
            width: parent.width
            height: parent.width

            Image {
                height: parent.height
                width: parent.height
                id: background
                source: "clock.png"
                visible: clock.night == false
            }
            Image {
                height: parent.height
                width: parent.height
                source: "clock-night.png"
                visible: clock.night == true
            }

            Image {
                x: (parent.width / 2)+(width/2)
                y: parent.height / 2
                height: parent.height / 2 * .8
                width: height * .05
                source: "hour.png"
                transform: Rotation {
                    id: hourRotation
                    origin.x: 0
                    origin.y: 0
                    angle: (clock.hours * 30) + (clock.minutes * 0.5) + 180
                    Behavior on angle {
                        SpringAnimation {
                            spring: 2
                            damping: 0.2
                            modulus: 360
                        }
                    }
                }
            }

            Image {
                x: (parent.width / 2) +(width/2)
                y: (parent.height / 2)
                height: parent.height / 2 * .9
                width: height * .025
                source: "minute.png"
                transform: Rotation {
                    id: minuteRotation
                    origin.x: 0
                    origin.y: 0
                    angle: clock.minutes * 6 + 180
                    Behavior on angle {
                        SpringAnimation {
                            spring: 2
                            damping: 0.2
                            modulus: 360
                        }
                    }
                }
            }

            Image {
                x: (parent.width / 2)+(width/2)
                y: (parent.height / 2)
                height: parent.height / 2 * .9
                width: 1
                source: "second.png"
                transform: Rotation {
                    id: secondRotation
                    origin.x: 0
                    origin.y: 0
                    angle: clock.seconds * 6 + 180
                    Behavior on angle {
                        SpringAnimation {
                            spring: 2
                            damping: 0.2
                            modulus: 360
                        }
                    }
                }
            }

            Image {
                height: parent.height * .1
                width: parent.height * .1
                anchors.centerIn: background
                source: "center.png"
            }
        }
    }
    Text {
        id: txtDate
        x: 0
        y: 0
        width: parent.width
        height: parent.height/32
        color: "#000000"
        text: qsTr("Load")
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height/32
    }
}
